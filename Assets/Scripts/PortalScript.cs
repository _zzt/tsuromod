﻿using System.Collections;
using UnityEngine;

public class PortalScript : MonoBehaviour
{
    //[HideInInspector]
    public TileScript LocatedTile;

    //[HideInInspector]
    public Direction Direction;

    public PortalScript ConnectedTo { get; private set; }

    public void ConnectTo(PortalScript portal)
    {
        Debug.Assert(portal.ConnectedTo == null && ConnectedTo == null);
        portal.ConnectedTo = this;
        ConnectedTo = portal;
    }

    private void Update()
    {
        if (GameManagerScript.Instance.GameState != GameState.NotStarted)
            transform.Rotate(0, 0, Time.deltaTime * 100);
    }
}