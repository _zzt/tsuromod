﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    // Singleton with Assertion - Any access to the instance before Awake() fails.

    private static PlayerScript instance;

    public static PlayerScript Instance
    {
        get
        {
            Debug.Assert(instance != null);
            return instance;
        }
    }

    private void Awake()
    {
        Debug.Assert(instance == null);
        instance = this;
    }

    [HideInInspector]
    public float Speed = 1;

    [SerializeField]
    private List<Sprite> PlayerSprites;

    private IEnumerator Start()
    {
        while (GameManagerScript.Instance.GameState != GameState.Playing)
            yield return null;

        int cur_index = 0;

        while (true)
        {
            yield return new WaitForSeconds(0.02f);
            if (++cur_index == PlayerSprites.Count) cur_index = 0;
            GetComponent<SpriteRenderer>().sprite = PlayerSprites[cur_index];
        }
    }
}
