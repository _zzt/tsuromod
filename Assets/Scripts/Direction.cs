﻿using UnityEngine;

public enum Direction
{
    Down = 0,
    Right,
    Up,
    Left,
}

public static class DirectionHelper
{
    public static Direction Rotate(this Direction direction, TileRotation rotation)
    {
        return (Direction)(((TileRotation)direction).AddRotation(rotation));
    }

    public static Direction Opposite(this Direction direction)
    {
        return (Direction)(((int)direction + 2) % 4);
    }

    public static Vector2 Vectorize(this Direction direction)
    {
        switch (direction)
        {
            case Direction.Down:
                return Vector2.down;
            case Direction.Right:
                return Vector2.right;
            case Direction.Up:
                return Vector2.up;
            case Direction.Left:
                return Vector2.left;
            default:
                throw new System.NotImplementedException();
        }
    }

    public static Direction RandomDirection()
    {
        return (Direction)Random.Range(0, 4);
    }
}
