﻿public enum GameState
{
    NotStarted,
    Playing,
    Dead,
    Paused,
    InAPortal,
}
