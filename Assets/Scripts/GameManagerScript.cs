﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{

    private static GameManagerScript instance;

    public static GameManagerScript Instance
    {
        get
        {
            Debug.Assert(instance != null);
            return instance;
        }
    }

    public GameState GameState { get; private set; }

    [SerializeField]
    int GridWidth = 5;

    [SerializeField]
    int GridHeight = 5;

    public float TileSize = 1;

    public Sprite StraightLineTileImage;
    public Sprite CurvedLineTileImage;
    public Sprite DoubleCurvedLineTileImage;

    public GameObject BorderObject;
    public GameObject TileObject;
    public GameObject PortalObject;

    public Text CenterMessageText;
    public Text ScoreText;
    private int score = 0;

    public GameObject PausePanel;
    public GameObject GameOverPanel;

    TileScript[,] tiles;
    TileScript[,] Tiles
    {
        get
        {
            Debug.Assert(tiles != null);
            return tiles;
        }
    }

    int playerX, playerY;

    public TileScript CurrentTile
    {
        get
        {
            return tiles[playerX, playerY];
        }
    }

    float progressInTile = 0;
    Direction currentTileEnteredFrom = Direction.Down;

    public float StartingSpeed = 1;
    public float Acceleration = 0.1f;
    public int AccelerateForEvery = 3;

    private int countUntilNextAccel = 0;

    public int TileInitialDurability = 2;

    [HideInInspector]
    public List<PortalScript> Portals = new List<PortalScript>();

    GameState gameStateBeforePause;

    public AudioClip PlayerDeathSound;
    public AudioClip PortalWarpSound;

    public AudioSource AudioSource;

    private void Awake()
    {
        // Set up the singleton instance
        Debug.Assert(instance == null);
        instance = this;

        GameState = GameState.NotStarted;

        // Set up tiles
        float startingX = (GridWidth - 1) * TileSize * -0.5f;
        float startingY = (GridHeight - 1) * TileSize * -0.5f;

        tiles = new TileScript[GridWidth, GridHeight];
        for (int i = 0; i < GridWidth; i++)
            for (int j = 0; j < GridHeight; j++)
            {
                TileScript newTile = tiles[i, j] =
                    Instantiate(TileObject, new Vector2(startingX + i * TileSize, startingY + j * TileSize), Quaternion.identity).GetComponent<TileScript>();

                newTile.X = i;
                newTile.Y = j;

                HashSet<Direction> arrows = new HashSet<Direction>();
                if (i == 0)
                    arrows.Add(Direction.Left);
                if (i == GridWidth - 1)
                    arrows.Add(Direction.Right);
                if (j == 0)
                    arrows.Add(Direction.Down);
                if (j == GridHeight - 1)
                    arrows.Add(Direction.Up);

                newTile.Position += arrows.Count;

                newTile.Setup(TileTypeHelper.RandomType(newTile.Position), TileRotationHelper.RandomRotation());

                foreach (ArrowScript arrow in newTile.GetComponentsInChildren<ArrowScript>())
                {
                    Direction rotatedDirection = arrow.Direction.Rotate(newTile.Rotation);
                    if (!arrows.Contains(rotatedDirection) || !newTile.CheckEntryAvailable(rotatedDirection))
                        arrow.gameObject.SetActive(false);
                }
            }
    }

    private void Start()
    {
        PlayerScript.Instance.Speed = StartingSpeed;
        CreateTwoPortals();
    }

    bool CheckPaused() { return GameState == GameState.Paused; }

    public void ChooseStartingPosition(TileScript startingTile, Direction direction)
    {
        if (GameState == GameState.Paused) return;
        foreach (TileScript tile in Tiles)
            foreach (Transform child in tile.transform)
                if (child.GetComponent<PortalScript>() == null) Destroy(child.gameObject);

        playerX = startingTile.X;
        playerY = startingTile.Y;
        currentTileEnteredFrom = direction;
        PlayerScript.Instance.GetComponent<SpriteRenderer>().color = Color.white;
        PlayerScript.Instance.transform.position = CurrentTile.GetPositionByProgress(0, currentTileEnteredFrom);
        PlayerScript.Instance.transform.rotation = CurrentTile.GetRotationByProcess(0, currentTileEnteredFrom);
        StartCoroutine(CountDownAndStartGame());
    }

    private IEnumerator CountDownAndStartGame()
    {
        CenterMessageText.text = "Three";
        yield return new WaitForSeconds(1f);
        yield return new WaitWhile(CheckPaused);
        CenterMessageText.text = "Two";
        yield return new WaitForSeconds(1f);
        yield return new WaitWhile(CheckPaused);
        CenterMessageText.text = "One";
        yield return new WaitForSeconds(1f);
        yield return new WaitWhile(CheckPaused);
        CenterMessageText.text = "";
        GameState = GameState.Playing;
    }

    private IEnumerator PlayDeathAnimations()
    {
        float elapsedtime = 0;
        float crashingduration = 0.6f / PlayerScript.Instance.Speed;

        while (elapsedtime < crashingduration)
        {
            elapsedtime += Time.deltaTime;
            PlayerScript.Instance.transform.position += PlayerScript.Instance.transform.up * Time.deltaTime * PlayerScript.Instance.Speed * (1 - elapsedtime / crashingduration);
            yield return new WaitForEndOfFrame();
        }

        PlayAudio(PlayerDeathSound);

        const float turningredduration = 0.5f;
        const float fadingduration = 0.3f;
        //CenterMessageText.text = "YOU DIED";
        SpriteRenderer renderer = PlayerScript.Instance.GetComponent<SpriteRenderer>();
        while (elapsedtime < turningredduration)
        {
            elapsedtime += Time.deltaTime;
            renderer.color = new Color(1, 1 - elapsedtime / turningredduration, 1 - elapsedtime / turningredduration);
            yield return new WaitForEndOfFrame();
        }
        elapsedtime = 0;
        while (elapsedtime < fadingduration)
        {
            elapsedtime += Time.deltaTime;
            renderer.color = new Color(1, 0, 0, 1 - elapsedtime / fadingduration);
            yield return new WaitForEndOfFrame();
        }
        //yield return new WaitForSeconds(3 - turningredduration - fadingduration);

        GameOverPanel.SetActive(true);
    }

    void CreateOnePortal()
    {
        while (true)
        {
            loopbegin:
            int x = UnityEngine.Random.Range(0, GridWidth);
            int y = UnityEngine.Random.Range(0, GridHeight);
            Direction direction = DirectionHelper.RandomDirection();
            if (Tiles[x, y].CheckEntryAvailable(direction.Rotate(tiles[x, y].Rotation))) continue;
            foreach (PortalScript portal in Portals)
                if (portal.LocatedTile == Tiles[x, y] && portal.Direction == direction) goto loopbegin;
            PortalScript newportal = Instantiate(PortalObject, tiles[x, y].transform, false).GetComponent<PortalScript>();
            newportal.Direction = direction;
            newportal.LocatedTile = Tiles[x, y];
            newportal.transform.localPosition = direction.Vectorize() * TileSize * 0.3f;
            Portals.Add(newportal);
            return;
        }
    }

    void CreateTwoPortals()
    {
        CreateOnePortal();
        CreateOnePortal();
        Portals[Portals.Count - 1].ConnectTo(Portals[Portals.Count - 2]);
    }

    public void Pause()
    {
        if (GameState == GameState.Playing || GameState == GameState.NotStarted || GameState == GameState.InAPortal)
        {
            gameStateBeforePause = GameState;
            GameState = GameState.Paused;
            PausePanel.SetActive(true);
        }
        else if (GameState == GameState.Paused) Resume();
    }

    public void ToMenuScene()
    {
        SceneManager.LoadScene("Menu Scene");
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main Scene");
    }

    public void Resume()
    {
        Debug.Assert(GameState == GameState.Paused);
        PausePanel.SetActive(false);
        GameState = gameStateBeforePause;
    }

    private void Update()
    {
        switch (GameState)
        {
            case GameState.Playing:
                progressInTile += Time.deltaTime * PlayerScript.Instance.Speed;
                while (progressInTile >= 1)
                {
                    Direction nextTileEntryDirection = CurrentTile.GetNextDirection(currentTileEnteredFrom).Opposite();
                    TileScript previousTile = CurrentTile;

                    if (portaljumped)
                    {
                        previousTile = exitingFrom.LocatedTile;
                        nextTileEntryDirection = exitingFrom.Direction.Opposite().Rotate(exitingFrom.LocatedTile.Rotation);

                        //playerX = exitingFrom.LocatedTile.X;
                        //playerY = exitingFrom.LocatedTile.Y;
                        PlayerScript.Instance.transform.rotation = Quaternion.Euler(((TileRotation)nextTileEntryDirection).GetEulerAngle());
                        PlayerScript.Instance.transform.position = exitingFrom.LocatedTile.transform.position +
                            (Vector3)(nextTileEntryDirection.Opposite().Vectorize() * 0.5f * TileSize);
                        Portals.Remove(exitingFrom.ConnectedTo);
                        Portals.Remove(exitingFrom);
                        Destroy(exitingFrom.ConnectedTo.gameObject);
                        Destroy(exitingFrom.gameObject);
                        Debug.Assert(Portals.Count == 0);
                        CreateTwoPortals();
                        portaljumped = false;
                    }

                    switch (nextTileEntryDirection.Opposite())
                    {
                        case Direction.Down:
                            playerY -= 1;
                            break;

                        case Direction.Right:
                            playerX += 1;
                            break;

                        case Direction.Up:
                            playerY += 1;
                            break;

                        case Direction.Left:
                            playerX -= 1;
                            break;

                        default: throw new Exception();
                    }
                    if (playerX < 0 || playerX >= GridWidth || playerY < 0 || playerY >= GridHeight
                        || !CurrentTile.CheckEntryAvailable(nextTileEntryDirection))
                    {
                        if (!(playerX < 0 || playerX >= GridWidth || playerY < 0 || playerY >= GridHeight))
                            // Check if there's a portal
                            for (int i = 0; i < Portals.Count; i++)
                            {
                                PortalScript portal = Portals[i];
                                if (portal.LocatedTile == CurrentTile && portal.Direction.Rotate(CurrentTile.Rotation) == nextTileEntryDirection)
                                {
                                    StartCoroutine(PortalJump(portal));
                                    GameState = GameState.InAPortal;
                                    return;
                                }
                            }

                        // Died
                        GameState = GameState.Dead;
                        StartCoroutine(PlayDeathAnimations());

                        return;
                    }
                    else
                    {
                        // Moving on to the next tile
                        bool tilehasaportal = false;
                        foreach (PortalScript portal in Portals)
                            if (portal.LocatedTile == previousTile) tilehasaportal = true;

                        if (!tilehasaportal && --previousTile.DurabilityLeft <= 0)
                            previousTile.Setup(TileTypeHelper.RandomType(previousTile.Position), TileRotationHelper.RandomRotation());

                        progressInTile -= 1;
                        ScoreText.text = "Score : " + ++score;
                        if (++countUntilNextAccel >= AccelerateForEvery)
                        {
                            countUntilNextAccel = 0;
                            PlayerScript.Instance.Speed += Acceleration;
                        }
                        currentTileEnteredFrom = nextTileEntryDirection.Rotate(CurrentTile.Rotation.Inverse());
                    }
                }
                PlayerScript.Instance.transform.position = CurrentTile.GetPositionByProgress(progressInTile, currentTileEnteredFrom);
                PlayerScript.Instance.transform.rotation = CurrentTile.GetRotationByProcess(progressInTile, currentTileEnteredFrom);
                break;

            default:
                break;
        }
    }

    bool portaljumped = false;
    PortalScript exitingFrom;

    IEnumerator PortalJump(PortalScript portal)
    {
        PlayAudio(PortalWarpSound);
        float elapsedtime = 0;

        const float gettingindelay = 2f;
        const float gettingoutdelay = 2f;

        Vector3 startingPos = PlayerScript.Instance.transform.position;
        Vector3 endPos = portal.transform.position;

        while (elapsedtime <= gettingindelay)
        {
            elapsedtime += Time.deltaTime;
            PlayerScript.Instance.transform.position = Vector3.Lerp(startingPos, endPos, elapsedtime / gettingindelay);
            PlayerScript.Instance.transform.localScale = Vector3.one * (1 - elapsedtime / gettingindelay);
            PlayerScript.Instance.transform.Rotate(0, 0, 720 * Time.deltaTime / gettingindelay);
            yield return new WaitWhile(CheckPaused);
            yield return new WaitForEndOfFrame();
        }

        elapsedtime = 0;

        playerX = portal.ConnectedTo.LocatedTile.X;
        playerY = portal.ConnectedTo.LocatedTile.Y;

        Direction nextTileEntryDirection = portal.ConnectedTo.Direction.Opposite().Rotate(portal.ConnectedTo.LocatedTile.Rotation);

        startingPos = portal.ConnectedTo.transform.position = portal.ConnectedTo.transform.position;
        endPos = portal.ConnectedTo.LocatedTile.transform.position +
            (Vector3)(nextTileEntryDirection.Opposite().Vectorize() * 0.5f * TileSize);

        PlayerScript.Instance.transform.rotation = Quaternion.Euler(((TileRotation)nextTileEntryDirection).GetEulerAngle());

        while (elapsedtime <= gettingoutdelay)
        {
            elapsedtime += Time.deltaTime;
            PlayerScript.Instance.transform.position = Vector3.Lerp(startingPos, endPos, elapsedtime / gettingoutdelay);
            PlayerScript.Instance.transform.localScale = Vector3.one * elapsedtime / gettingoutdelay;
            PlayerScript.Instance.transform.Rotate(0, 0, 720 * Time.deltaTime / gettingoutdelay);
            yield return new WaitWhile(CheckPaused);
            yield return new WaitForEndOfFrame();
        }

        portaljumped = true;
        exitingFrom = portal.ConnectedTo;
        GameState = GameState.Playing;

    }

    public void PlayAudio(AudioClip clip)
    {
        OptionsManager oM = FindObjectOfType<OptionsManager>();
        if (oM != null && !oM.isMute)
        {
            AudioSource.clip = clip;
            AudioSource.Play();
        }
    }
}