﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

	private GameObject menuCanvas, instructionCanvas, creditsCanvas, startCanvas;
	private GameObject[] canvases;

	private OptionsManager optionsManager;

	void Awake() {
		FindCanvas();
		canvases = new GameObject[] {menuCanvas, instructionCanvas, creditsCanvas, startCanvas};
	}

	void Start() {
		for (int i = 0; i < canvases.Length; i++) {
			if (canvases[i]) {
				DeactivateCanvas(canvases[i]);
			}
		}

		optionsManager = FindObjectOfType<OptionsManager>();
	}

	public void LoadMain() {
		if (optionsManager.isFirstPlay) {
			ActivateCanvas(startCanvas);
			optionsManager.SetForceInstructions(true);
			optionsManager.UpdateFirstPlay();
		} else {
			optionsManager.SetForceInstructions(false);
			SceneManager.LoadScene("Main Scene");
		}
	}

	public void LoadMenu() {
		SceneManager.LoadScene("Menu Scene");
	}

	public void LoadCredits() {
		SceneManager.LoadScene("Credits Scene");
	}

	public void QuitApplication() {
		Application.Quit();
	}

	public void ToggleAudioMute() {
		if (optionsManager != null) {
			optionsManager.ToggleMute();
		}
	}

	public void PlayAudio() {
		if (optionsManager != null) {
			optionsManager.PlayButtonClick ();
		}
	}

	public void ActivateCanvas(GameObject canvas) {
		canvas.SetActive(true);
	}

	public void DeactivateCanvas(GameObject canvas) {
		canvas.SetActive(false);
	}

	public void FindCanvas() {
		menuCanvas = GameObject.FindWithTag("Menu");
		instructionCanvas = GameObject.FindWithTag("Instructions");
		creditsCanvas = GameObject.FindWithTag("Credits");
		startCanvas = GameObject.FindWithTag("Start");
	}
}
