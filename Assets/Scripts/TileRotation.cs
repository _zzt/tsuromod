﻿using UnityEngine;

public enum TileRotation
{
    CCW0 = 0,
    CCW90,
    CCW180,
    CCW270
}

public static class TileRotationHelper
{
    public static TileRotation AddRotation(this TileRotation currentRotation, TileRotation rotationAmount)
    {
        return (TileRotation)(((int)currentRotation + (int)rotationAmount) % 4);
    }

    public static TileRotation Inverse(this TileRotation currentRotation)
    {

        switch (currentRotation)
        {
            case TileRotation.CCW90:
                return TileRotation.CCW270;
            case TileRotation.CCW270:
                return TileRotation.CCW90;
            default:
                return currentRotation;
        }
    }

    public static TileRotation RandomRotation()
    {
        return (TileRotation)Random.Range(0, 4);
    }

    public static Vector3 GetEulerAngle(this TileRotation tileRotation)
    {
        return new Vector3(0, 0, (int)tileRotation * 90);
    }
}