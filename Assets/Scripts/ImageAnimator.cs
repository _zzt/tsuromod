﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ImageAnimator : MonoBehaviour {

	public float fps;

	private int i;
	private Image mat;
	private Sprite[] frames;

	void Awake() {
		mat = this.GetComponent<Image>();
	}
		
	void Update() {
		if (frames != null) {
			i = (int)(frames.Length - 1 - (Time.time * fps) % frames.Length);
			/*
			if (i > 0) {
				i--;
			} else {
				i = frames.Length - 1;
			}
			*/
			mat.sprite = frames[i];
		}
	}

	public void setFrames(Sprite[] input) {
		frames = input;
		i = frames.Length - 1;
		mat.sprite = frames[i];
	}
}
