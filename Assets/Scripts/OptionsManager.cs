﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionsManager : MonoBehaviour {

	private static OptionsManager instance;

	public AudioSource bgm;
	public AudioSource buttonClick;
	private AudioSource[] allAudio;

    [HideInInspector]
	public bool isMute = false;
	public bool isFirstPlay = true;
	public bool isForceInstructions = false;

	void Awake() {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(this.gameObject);
		}
		DontDestroyOnLoad(this);
	}

	void Start() {
		bgm.Play();
	}

	public void UpdateFirstPlay() {
		isFirstPlay = false;
	}

	public void SetForceInstructions(bool value) {
		isForceInstructions = value;
	}

	public void PlayButtonClick() {
		buttonClick.Play();
	}

	public void ToggleMute() {
		isMute = !isMute;

		bgm.mute = isMute;
		buttonClick.mute = isMute;
	}
}
