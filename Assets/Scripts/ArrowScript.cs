﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowScript : MonoBehaviour
{
    public Direction Direction;

    private void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.75f);
    }

    private void OnMouseUpAsButton()
    {
        GameManagerScript.Instance.ChooseStartingPosition(GetComponentInParent<TileScript>(), Direction);
    }
}
