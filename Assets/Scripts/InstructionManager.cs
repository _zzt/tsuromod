﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionManager : MonoBehaviour {

	private OptionsManager optionsManager;
	private ImageAnimator image;
	private Text imageText;

	private bool canLoad = false;
	private int page = 0;
	private int maxPage = 3;

	public Sprite[] instruction0;
	public Sprite[] instruction1;
	public Sprite[] instruction2;
	public Sprite[] instruction3;
	private Sprite[][] instructions;

	private string text0;
	private string text1;
	private string text2;
	private string text3;
	private string[] texts;

	public GameObject menuButton;
	public GameObject playButton;
	public GameObject prevButton;
	public GameObject nextButton;

	void Awake () {
		optionsManager = FindObjectOfType<OptionsManager>();
		image = this.GetComponentInChildren<ImageAnimator>();	
		imageText = this.GetComponentInChildren<Text>();
		instructions = new Sprite[][]{ instruction0, instruction1, instruction2, instruction3 };

		text0 = "Left click a tile to rotate it counter-clockwise\nRight click a tile to rotate it clockwise\nYou may not rotate the tile you are currently on";
		text1 = "Stay on the path to stay alive";
		text2 = "Wormholes can be used to travel between different areas";
		text3 = "Paths may change over time";
		texts = new string[]{ text0, text1, text2, text3 };
	}

	void OnEnable() {
		if (canLoad) {
			page = 0;
			loadPage(page);

			if (optionsManager.isForceInstructions) {
				menuButton.SetActive(false);
			}
		}
	}

	void OnDisable() {
		canLoad = true;
	}

	void loadPage(int i) {
		image.setFrames(instructions[i]);
		imageText.text = texts[i];

		if (i == 0) {
			prevButton.SetActive (false);
			nextButton.SetActive (true);
		} else if (i == maxPage) {
			nextButton.SetActive (false);
			prevButton.SetActive (true);
			if (optionsManager.isForceInstructions) {
				playButton.SetActive (true);
			}
		} else {
			prevButton.SetActive (true);
			nextButton.SetActive (true);
		}
	}

	public void nextPage() {
		if (page < maxPage) {
			page++;
			loadPage(page);

            if (page == maxPage)
                FindObjectOfType<OptionsManager>().isFirstPlay = false;

        }
	}

	public void previousPage() {
		if (page > 0) {
			page--;
			loadPage(page);
		}
	}
}
