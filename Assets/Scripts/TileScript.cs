﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{
    private TileRotation rotation;

    public TileRotation Rotation
    {
        get
        {
            return rotation;
        }

        set
        {
            rotation = value;
        }
    }

    public TileType Type { get; private set; }
    public TilePosition Position = TilePosition.Center;

    public int X, Y;

    bool setupdone = false;
    bool rotating = false;

    private int durabilityLeft;
    public int DurabilityLeft
    {
        get
        {
            return durabilityLeft;
        }
        set
        {
            durabilityLeft = value;
        }
    }

    void Start()
    {
        Debug.Assert(setupdone);
    }
    
    public void Setup(TileType type, TileRotation rotation)
    {
        Type = type;
        Rotation = rotation;
        DurabilityLeft = GameManagerScript.Instance.TileInitialDurability;
        if (setupdone)
            StartCoroutine(PlayResetAnimation());
        else
        {
            setupdone = true;
            GetComponent<SpriteRenderer>().sprite = type.GetTileImage();
        }
        transform.rotation = Quaternion.Euler(rotation.GetEulerAngle());
    }

    IEnumerator PlayResetAnimation()
    {
        float elapsedtime = 0;
        const float fadingduration = 0.3f;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        while (elapsedtime < fadingduration)
        {
            elapsedtime += Time.deltaTime;
            renderer.color = new Color(1, 1, 1, (1 - elapsedtime / fadingduration));
            yield return new WaitForEndOfFrame();
        }
        elapsedtime = 0;
        GetComponent<SpriteRenderer>().sprite = Type.GetTileImage();
        while (elapsedtime < fadingduration)
        {
            elapsedtime += Time.deltaTime;
            renderer.color = new Color(1, 1, 1, elapsedtime / fadingduration);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnMouseExit()
    {
        GameManagerScript.Instance.BorderObject.SetActive(false);
    }
    
    private void OnMouseOver()
    {
        if (GameManagerScript.Instance.GameState != GameState.Playing && GameManagerScript.Instance.GameState != GameState.InAPortal) return;

        if (!GameManagerScript.Instance.BorderObject.activeInHierarchy)
        {
            GameManagerScript.Instance.BorderObject.SetActive(true);
            GameManagerScript.Instance.BorderObject.transform.position = transform.position;
        }
        else
            GameManagerScript.Instance.BorderObject.GetComponent<SpriteRenderer>().color =
                GameManagerScript.Instance.CurrentTile == this ? new Color(1, 0.5f, 0.5f) : Color.white;

        if (!rotating && (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1)))
        {
            if (GameManagerScript.Instance.CurrentTile == this)
                return;

            if (Input.GetMouseButtonUp(0))
                Rotation = Rotation.AddRotation(TileRotation.CCW90);
            else
                Rotation = Rotation.AddRotation(TileRotation.CCW270);

            StartCoroutine(AnimateRotation());
        }
    }

    IEnumerator AnimateRotation()
    {
        const float rotationduration = 0.1f;
        float elapsedtime = 0;
        rotating = true;
        Quaternion start = transform.rotation;
        Quaternion end = Quaternion.Euler(rotation.GetEulerAngle());
        while (elapsedtime < rotationduration)
        {
            elapsedtime += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(start, end, elapsedtime / rotationduration);
            yield return new WaitForEndOfFrame();
        }
        rotating = false;
    }

    const float straightAngle = Mathf.PI * 0.5f;

    public Vector3 GetPositionByProgress(float progress, Direction enteredFrom)
    {
        switch (Type) // hard-coded for now
        {
            case TileType.StraightLine:
                Debug.Assert(enteredFrom == Direction.Down || enteredFrom == Direction.Up);
                return transform.TransformPoint(new Vector3(0, GameManagerScript.Instance.TileSize * (0.5f - progress) * (enteredFrom == Direction.Down ? -1 : 1), 0)
                    * GameManagerScript.Instance.TileSize);

            case TileType.Curved:
            case TileType.DoubleCurved:
                float signedprogress;
                if (enteredFrom == Direction.Down || enteredFrom == Direction.Right)
                {
                    signedprogress = (enteredFrom == Direction.Down ? progress : (1 - progress));
                    return transform.TransformPoint(new Vector3(
                        -Mathf.Cos(straightAngle * signedprogress), Mathf.Sin(straightAngle * signedprogress), 0) * 0.5f
                        + new Vector3(0.5f, -0.5f, 0) * GameManagerScript.Instance.TileSize);
                }
                else
                {
                    Debug.Assert(Type != TileType.Curved);
                    signedprogress = (enteredFrom == Direction.Up ? progress : (1 - progress));
                    return transform.TransformPoint(new Vector3(
                        Mathf.Cos(straightAngle * signedprogress), -Mathf.Sin(straightAngle * signedprogress), 0) * 0.5f
                        + new Vector3(-0.5f, 0.5f, 0) * GameManagerScript.Instance.TileSize);
                }

            default:
                Debug.LogError("Other Functionalities Not Implemented Yet");
                throw new NotImplementedException();
        }
    }

    public Quaternion GetRotationByProcess(float progress, Direction enteredFrom)
    {
        switch (Type) // hard-coded for now
        {
            case TileType.StraightLine:
                Debug.Assert(enteredFrom == Direction.Down || enteredFrom == Direction.Up);
                return transform.rotation * Quaternion.Euler(0, 0, enteredFrom == Direction.Down ? 0 : 180);

            case TileType.Curved:
            case TileType.DoubleCurved:
                if (enteredFrom == Direction.Down || enteredFrom == Direction.Right)
                    return transform.rotation * Quaternion.Euler(0, 0, enteredFrom == Direction.Down ? -90 * progress : 90 * (1 + progress));
                else
                    return transform.rotation * Quaternion.Euler(0, 0, enteredFrom == Direction.Up ? 90 * (2 - progress) : -90 * (1 - progress));

            default:
                Debug.LogError("Other Functionalities Not Implemented Yet");
                throw new NotImplementedException();
        }
    }

    public Direction GetNextDirection(Direction enteredFrom)
    {
        Direction unrotatedDirection;
        switch (Type)
        {
            case TileType.StraightLine:
                unrotatedDirection = enteredFrom == Direction.Up ? Direction.Down : Direction.Up;
                break;

            case TileType.DoubleCurved:
            case TileType.Curved:
                switch (enteredFrom)
                {
                    case Direction.Down:
                        unrotatedDirection = Direction.Right;
                        break;

                    case Direction.Right:
                        unrotatedDirection = Direction.Down;
                        break;

                    case Direction.Up:
                        unrotatedDirection = Direction.Left;
                        break;

                    case Direction.Left:
                        unrotatedDirection = Direction.Up;
                        break;

                    default:
                        throw new Exception();
                }
                break;

            default:
                Debug.LogError("Other Functionalities Not Implemented Yet");
                throw new NotImplementedException();
        }

        return unrotatedDirection.Rotate(Rotation);
    }

    public bool CheckEntryAvailable(Direction entryDirection)
    {
        Direction rotatedEentryDirection = entryDirection.Rotate(Rotation.Inverse());
        switch (Type)
        {
            case TileType.StraightLine:
                return rotatedEentryDirection == Direction.Down || rotatedEentryDirection == Direction.Up;

            case TileType.Curved:
                return rotatedEentryDirection == Direction.Down || rotatedEentryDirection == Direction.Right;

            case TileType.DoubleCurved: return true;

            default:
                Debug.LogError("Other Functionalities Not Implemented Yet");
                throw new NotImplementedException();
        }
    }
}
