﻿using UnityEngine;

public enum TileType
{
    Curved,
    DoubleCurved,
    StraightLine,
}

public static class TileTypeHelper
{
    public static TileType RandomType(TilePosition position)
    {
        switch (position)
        {
            case TilePosition.Center:
                return (TileType)Random.Range(0, 3);

            case TilePosition.Edge:
                if (Random.Range(0, 10) == 0) return TileType.StraightLine;
                else return (TileType)Random.Range(0, 2);

            case TilePosition.Corner:
                return (TileType)Random.Range(0, 2);

            default:
                throw new System.NotImplementedException();
        }

    }

    public static Sprite GetTileImage(this TileType tileType)
    {
        switch (tileType)
        {
            case TileType.StraightLine:
                return GameManagerScript.Instance.StraightLineTileImage;

            case TileType.Curved:
                return GameManagerScript.Instance.CurvedLineTileImage;

            case TileType.DoubleCurved:
                return GameManagerScript.Instance.DoubleCurvedLineTileImage;

            default:
                Debug.LogError("Unreachable Part Reached");
                return null;
        }
    }
}